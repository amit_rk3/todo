import React from 'react';
import './App.css';
import ToDo from './components/ToDo';

function App() {
  return (
    <div className="App container justify-content-center">
      <ToDo />
    </div>
  );
}

export default App;

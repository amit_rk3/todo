import React, { useState } from 'react';
import './ToDo.css';

const Status = {
    COMPLETE: 'complete',
    ACTIVE: 'active',
}

const ToDo = () => {

    const [toDo, setToDo] = useState({ value: '', status: Status.ACTIVE });
    const [toDoList, setToDoList] = useState([]);
    const [showStatus, setShowStatus] = useState('');

    const addToDo = (todo) => {
        if (!todo.value) {
            alert('Enter a value');
            return false;
        }

        setToDoList([...toDoList, todo]);
        setToDo({ value: '' });
    }

    const setStatus = (index, status) => {
        const newToDoList = [...toDoList];
        newToDoList[index].status = status ? Status.COMPLETE : Status.ACTIVE;
        setToDoList(newToDoList);
    }

    return (
        <>
        <h1 className="mt-5">ToDos</h1>
        {/* main add todo Section */}
        <div className="mt-2 ml-5 todo-wrapper">
            <form style={{ width: '100%' }} onSubmit={e => {
                e.preventDefault();
                addToDo({ value: toDo.value, status: Status.ACTIVE });
            }}>

                {/* input for todo */}
                <div className="row">
                    <input type="text" className="form-control col-sm-12" placeholder="add todo..."
                        value={toDo.value} onChange={e => setToDo({ ...toDo, value: e.target.value })} />
                </div>


                {/* display todos by statuses */}

                {toDoList.filter(todo => showStatus === '' ? true : showStatus === todo.status).map((toDo, index) => {
                    return <div className="row text-left todo-list-item">
                        <div key={`todo-${index}`} className="col-sm-6">{toDo.status === Status.COMPLETE ? <del>{toDo.value}</del> : toDo.value}</div>
                        <div key={`todo-${index}`} className="col-sm-6 text-right">
                            <input type="checkbox" checked={toDo.status === Status.COMPLETE}
                                onChange={(e) => setStatus(index, e.target.checked)} /> Complete</div>
                    </div>;
                })}

                {/* filter todos by statuses */}
                <div className="row text-left todo-list-item">
                    <div className="col-lg-3">
                        <input name="showStatus" checked={showStatus === ''}
                            type="radio" onClick={() => setShowStatus('')} /> All
                        </div>
                    <div className="col-lg-5">
                        <input name="showStatus" checked={showStatus === Status.COMPLETE}
                            type="radio" onClick={() => setShowStatus(Status.COMPLETE)} /> Complete
                        </div>
                    <div className="col-lg-4">
                        <input name="showStatus" checked={showStatus === Status.ACTIVE}
                            type="radio" onClick={() => setShowStatus(Status.ACTIVE)} /> Active
                        </div>
                </div>
            </form>
        </div>
        </>
    );
}

export default ToDo;